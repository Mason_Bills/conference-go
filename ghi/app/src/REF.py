from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
import logging

from .encoders import (
    AutomobileEncoder,
    ManufacturerEncoder,
    VehicleModelEncoder,
)
from .models import Automobile, Manufacturer, VehicleModel
logger = logging.getLogger(__name__)


@require_http_methods(["GET", "POST"])
def api_automobiles(request):
    if request.method == "GET":
        autos = Automobile.objects.all()
        return JsonResponse(
            {"autos": autos},
            encoder=AutomobileEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            model_id = content["model_id"]
            model = VehicleModel.objects.get(pk=model_id)
            content["model"] = model
            auto = Automobile.objects.create(**content)
            return JsonResponse(
                auto,
                encoder=AutomobileEncoder,
                safe=False,
            )
        except Exception as e:
            logging.error(e)
            response = JsonResponse(
                {"message": f"Could not create the automobile: {str(e)})"},
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_automobile(request, vin):
    if request.method == "GET":
        try:
            auto = Automobile.objects.get(vin=vin)
            return JsonResponse(
                auto,
                encoder=AutomobileEncoder,
                safe=False
            )
        except Automobile.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            auto = Automobile.objects.get(vin=vin)
            auto.delete()
            return JsonResponse(
                auto,
                encoder=AutomobileEncoder,
                safe=False,
            )
        except Automobile.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            auto = Automobile.objects.get(vin=vin)

            props = ["color", "year", "sold"]
            for prop in props:
                if prop in content:
                    setattr(auto, prop, content[prop])
            auto.save()
            return JsonResponse(
                auto,
                encoder=AutomobileEncoder,
                safe=False,
            )
        except Automobile.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_manufacturers(request):
    if request.method == "GET":
        manufacturers = Manufacturer.objects.all()
        return JsonResponse(
            {"manufacturers": manufacturers},
            encoder=ManufacturerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            manufacturer = Manufacturer.objects.create(**content)
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the manufacturer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_manufacturer(request, pk):
    if request.method == "GET":
        try:
            manufacturer = Manufacturer.objects.get(id=pk)
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False
            )
        except Manufacturer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            manufacturer = Manufacturer.objects.get(id=pk)
            manufacturer.delete()
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except Manufacturer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            manufacturer = Manufacturer.objects.get(id=pk)

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(manufacturer, prop, content[prop])
            manufacturer.save()
            return JsonResponse(
                manufacturer,
                encoder=ManufacturerEncoder,
                safe=False,
            )
        except Manufacturer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_vehicle_models(request):
    if request.method == "GET":
        models = VehicleModel.objects.all()
        return JsonResponse(
            {"models": models},
            encoder=VehicleModelEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            manufacturer_id = content["manufacturer_id"]
            # the struggle was real. finally figured out the url i was using was too long
            # Django default length is 200 characters
            print(manufacturer_id)
            url = "https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/2018_Nissan_GT-R_Premium_in_Super_Silver%2C_Front_Right%2C_10-11-2022.jpg/560px-2018_Nissan_GT-R_Premium_in_Super_Silver%2C_Front_Right%2C_10-11-2022.jpg"
            print(len(url)) # this printed 211
            # went in and changed the url field to have a larger length
            manufacturer = Manufacturer.objects.get(id=manufacturer_id)
            content["manufacturer"] = manufacturer
            model = VehicleModel.objects.create(**content)
            # model = VehicleModel.objects.create(manufacturer=manufacturer, name=content["name"], picture_url=content["picture_url"])
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False,
            )
        # This way of error handling is awesome!!
        # remember to import logger
        except Exception as e:
            logger.error(e)
            response = JsonResponse(
                
                {"message": f"Could not create the vehicle model: {str(e)}"} # this printed "value too long for type character varying(200)" in 
                                                                             # the console
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_vehicle_model(request, pk):
    if request.method == "GET":
        try:
            model = VehicleModel.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False
            )
        except VehicleModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = VehicleModel.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False,
            )
        except VehicleModel.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            model = VehicleModel.objects.get(id=pk)
            props = ["name", "picture_url"]
            for prop in props:
                if prop in content:
                    setattr(model, prop, content[prop])
            model.save()
            return JsonResponse(
                model,
                encoder=VehicleModelEncoder,
                safe=False,
            )
        except VehicleModel.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return JsonResponse({"message": "Does not exist"})



# CarCar

Team:

* Person 1 - Which microservice?
* Person 2 - Which microservice?

## Design

## Service microservice

The models that have been created for this project include an appointments model, a technician model and a automobileVO all of which create a data table in the database that contains properties you would expect to be associated with an appointment and a technician. If youve gone over to the models.py file and taken a look at the automobileVO model you might ask why it is so sparce in associated properties? Shouldn't an automobile have more characteristics? The short answer is the AutomoibileVO is a special model in the sense that it allows the Services microservice to send and recieve data from the inventory microservice's Automobile. The reason for the AutomobileVO only having a few properties is to only recieve necessary data; it essentially creates a point of reference to the Auotmobile model that is found in the inventory microservice. This point of refernce is created via a poller system. The poller reaches out to the inventory api end point to update any data specified in the poller code function. 

## Sales microservice

Through the sales microservice, I defined 4 models (Salesperson, Customer, Sale, and AutomobileVO) that when working together with encoders and view functions can create, read, and delete data from the database. Of these models, AutomobileVO interacts with a poller in order to get access the "vin" and "sold" fields from the automobile object inside of Inventory. Next, I created a React frontend that can perform all the same operations. As for how this uniquely interacts with Inventory, I made it so that my CreateSale component would send a PUT request to update the sold status of the automobile that is being sold upon form submission.